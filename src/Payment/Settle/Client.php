<?php

/*
 * This file is part of the OtkurBiz/ByteDance.
 *
 * (c)
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\Payment\Settle;

use ByteDance\Payment\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author
 */
class Client extends BaseClient
{
    /**
     * 请求分账
     * @param string $settleNumber
     * @param string $orderNumber
     * @param string $desc
     * @param array $optional
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function settle(string $settleNumber, string $orderNumber, string $desc = '分账', array $optional = [])
    {
        $params = array_merge([
            'out_settle_no' => $settleNumber,
            'out_order_no' => $orderNumber,
            'settle_desc' => $desc
        ], $optional);
        return $this->request('ecpay/v1/settle', $params);
    }

    /**
     * 请求分账
     * @param string $settleNumber
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $settleNumber, array $optional = [])
    {
        $params = array_merge([
            'out_settle_no' => $settleNumber
        ], $optional);
        return $this->request('ecpay/v1/settle', $params);
    }
}
