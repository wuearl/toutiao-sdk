<?php
/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\Payment;

use ByteDance\Kernel\ServiceContainer;
use Closure;

/**
 * Class Application.
 *
 * @property \ByteDance\Payment\Order\Client $order
 * @property \ByteDance\Payment\Refund\Client $refund
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Order\ServiceProvider::class,
        Refund\ServiceProvider::class
    ];

    /**
     * @var array
     */
    protected $defaultConfig = [
        'http' => [
            'base_uri' => 'https://developer.toutiao.com/api/apps/',
        ],
    ];
    /**
     * Set sub-merchant.
     *
     * @param string      $mchId
     * @param string|null $appId
     *
     * @return $this
     */
    public function setSubMerchant(string $mchId)
    {
        $this['config']->set('thirdparty_id', $mchId);

        return $this;
    }

    public function getKey()
    {
        return $this['config']->key;
    }

    /**
     * 支付回调
     * @param Closure $closure
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handlePaidNotify(Closure $closure)
    {
        return (new Notify\Paid($this))->handle($closure);
    }
}
