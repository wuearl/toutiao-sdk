<?php

/*
 * This file is part of the OtkurBiz/ByteDance.
 *
 * (c)
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\Payment\Refund;

use ByteDance\Payment\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author
 */
class Client extends BaseClient
{
    /**
     * @param string $number
     * @param string $refundNumber
     * @param int $refundFee
     * @param array $optional
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function byOutTradeNumber(string $number, string $refundNumber, int $refundFee, array $optional = [])
    {
        return $this->refund($refundNumber, $refundFee, array_merge($optional, ['out_order_no' => $number]));
    }

    /**
     * @param string $refundNumber
     * @param int $refundFee
     * @param array $optional
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function refund(string $refundNumber, int $refundFee, $optional = [])
    {
        $params = array_merge([
            'out_refund_no' => $refundNumber,
            'refund_amount' => $refundFee
        ], $optional);

        return $this->request('ecpay/v1/create_refund', $params);
    }
}
