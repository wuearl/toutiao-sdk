<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\Payment\Notify;

use Closure;
use ByteDance\Kernel\Exceptions\Exception;
use ByteDance\Kernel\Support;
use ByteDance\Payment\Kernel\Exceptions\InvalidSignException;
use Symfony\Component\HttpFoundation\Response;

abstract class Handler
{
    const SUCCESS = 'success';
    const FAIL = 'fail';

    /**
     * @var \ByteDance\Payment\Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $message;

    /**
     * @var string|null
     */
    protected $fail;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Check sign.
     * If failed, throws an exception.
     *
     * @var bool
     */
    protected $check = true;

    /**
     * Respond with sign.
     *
     * @var bool
     */
    protected $sign = false;

    /**
     * @param \ByteDance\Payment\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Handle incoming notify.
     *
     * @param \Closure $closure
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function handle(Closure $closure);

    /**
     * @param string $message
     */
    public function fail(string $message)
    {
        $this->fail = $message;
    }

    /**
     * @param array $attributes
     * @param bool $sign
     *
     * @return $this
     */
    public function respondWith(array $attributes, bool $sign = false)
    {
        $this->attributes = $attributes;
        $this->sign = $sign;

        return $this;
    }

    /**
     * Build xml and return the response to WeChat.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     */
    public function toResponse(): Response
    {
        $base = [
            'err_no' => is_null($this->fail) ? 0 : 400,
            'err_tips' => is_null($this->fail) ? static::SUCCESS : static::FAIL,
        ];
        return new Response(json_encode($base));
    }

    /**
     * Return the notify message from request.
     *
     * @return array
     *
     * @throws \ByteDance\Kernel\Exceptions\Exception
     */
    public function getMessage(): array
    {
        if (!empty($this->message)) {
            return $this->message;
        }

        try {
            $message = json_decode(strval($this->app['request']->getContent()), true);
        } catch (\Throwable $e) {
            throw new Exception('Invalid request json: ' . $e->getMessage(), 400);
        }

        if (!is_array($message) || empty($message)) {
            throw new Exception('Invalid request json.', 400);
        }

        if ($this->check) {
            $this->validate($message);
        }

        return $this->message = json_decode($message['msg'], true);
    }

    /**
     * Decrypt message.
     *
     * @param string $key
     *
     * @return string|null
     *
     * @throws \ByteDance\Kernel\Exceptions\Exception
     */
    public function decryptMessage(string $key)
    {
        $message = $this->getMessage();
        if (empty($message[$key])) {
            return null;
        }

        return Support\AES::decrypt(
            base64_decode($message[$key], true),
            md5($this->app['config']->key),
            '',
            OPENSSL_RAW_DATA,
            'AES-256-ECB'
        );
    }

    /**
     * Validate the request params.
     *
     * @param array $message
     *
     * @throws \ByteDance\Payment\Kernel\Exceptions\InvalidSignException
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     */
    protected function validate(array $message)
    {
        $sign = $message['msg_signature'];
        unset($message['msg_signature']);

        if (Support\generate_verify_sign($message, $this->app['config']->token) !== $sign) {
            throw new InvalidSignException();
        }
    }

    /**
     * @param mixed $result
     */
    protected function strict($result)
    {
        if (true !== $result && is_null($this->fail)) {
            $this->fail(strval($result));
        }
    }
}
