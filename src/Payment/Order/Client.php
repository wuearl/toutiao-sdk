<?php

/*
 * This file is part of the OtkurBiz/ByteDance.
 *
 * (c)
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\Payment\Order;

use ByteDance\Payment\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author
 */
class Client extends BaseClient
{
    /**
     * 下单
     * @param $params
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unify($params)
    {
        return $this->request('ecpay/v1/create_order', $params);
    }

    /**
     * 订单查询
     * @param $outOrderNo
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query($outOrderNo)
    {
        $params['out_order_no'] = $outOrderNo;
        return $this->request('ecpay/v1/query_order', $params);
    }
}
