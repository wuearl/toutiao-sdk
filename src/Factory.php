<?php
/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance;

/**
 * Class Factory.
 *
 * @method static \ByteDance\MiniProgram\Application    miniProgram(array $config)
 * @method static \ByteDance\Payment\Application        payment(array $config)
 */
class Factory
{
    /**
     * @param $name
     * @param array $config
     *
     * @return \ByteDance\Kernel\ServiceContainer
     */
    public static function make($name, array $config)
    {
        $namespace = Kernel\Support\Str::studly($name);
        $application = "\\ByteDance\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
