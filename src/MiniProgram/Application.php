<?php
/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\MiniProgram;

use ByteDance\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @property \ByteDance\MiniProgram\Auth\AccessToken $access_token
 * @property \ByteDance\MiniProgram\Auth\Client $auth
 * @property \ByteDance\MiniProgram\KVData\Client $kv
 * @property \ByteDance\MiniProgram\Encryptor $encryptor
 * @property \ByteDance\MiniProgram\QRCode\Client $qrcode
 * @property \ByteDance\MiniProgram\Message\Client $message
 * @property \ByteDance\MiniProgram\Payment\Client $payment
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Auth\ServiceProvider::class,
        KVData\ServiceProvider::class,
        QRCode\ServiceProvider::class,
        Message\ServiceProvider::class,
        Payment\ServiceProvider::class,
    ];
}
