<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\MiniProgram\Auth;

use ByteDance\Kernel\BaseClient;

/**
 * Class Auth.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * Get session info by code.
     * @param string $code
     * @param bool $anonymous
     * @return array|\ByteDance\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function session(string $code, bool $anonymous = false)
    {
        $params = [
            'appid'  => $this->app['config']['app_id'],
            'secret' => $this->app['config']['app_secret'],
        ];
        if ($anonymous) {
            $params['anonymous_code'] = $code;
        } else {
            $params['code'] = $code;
        }

        return $this->httpGet('api/apps/jscode2session', $params);
    }
}
