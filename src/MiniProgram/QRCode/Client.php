<?php

/*
 * This file is part of the OtkurBiz/ByteDance.
 *
 * (c)
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\MiniProgram\QRCode;

use ByteDance\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author
 */
class Client extends BaseClient
{
    /**
     * @param array $params
     * @return mixed|string
     * @throws \ByteDance\Kernel\Exceptions\InvalidArgumentException
     * @throws \ByteDance\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(array $params = [])
    {
        $response= $this->httpPostJson('api/apps/qrcode',$params);
        $responses = $response->getBody()->getContents();
        $res = @json_decode($responses,1);
        if ($res){
            return $res;
        }else{
            return $responses;
        }
    }
}
