<?php

/*
 * This file is part of the OtkurBiz/ByteDance.
 *
 * (c)
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace ByteDance\MiniProgram\Payment;

use ByteDance\Kernel\BaseClient;
use ByteDance\Kernel\Support;

/**
 * Class Client.
 *
 * @author
 */
class Client extends BaseClient
{
    /**
     * @param string $uid
     * @param string $outTradeNo
     * @param int $totalAmount
     * @param string $subject
     * @param array $optional
     * @return array
     */
    public function pay(string $uid, string $outTradeNo, int $totalAmount, string $subject, array $optional = [])
    {
        $appId = $this->app['config']['pay_app_id'];
        $mchId = $this->app['config']['mch_id'];
        $secretKey = $this->app['config']['key'];
        $params = [
            'app_id' => $appId,
            'merchant_id' => $mchId,
            'timestamp' => $time = (string)time(),
            'sign_type' => 'MD5',
            'out_order_no' => $outTradeNo,
            'total_amount' => $totalAmount,
            'product_code' => 'pay',
            'payment_type' => 'direct',
            'trade_type' => 'H5',
            'version' => '2.0',
            'currency' => 'CNY',
            'subject' => $subject,
            'body' => $optional['body'] ?? $subject,
            'uid' => $uid,
            'trade_time' => $time,
            'valid_time' => $optional['valid_time'] ?? '300',
            'notify_url' => $optional['notify_url'] ?? 'https://www.baidu.com'
        ];
        if (!empty($optional['wx_url'])) {
            $params['wx_type'] = 'MWEB';
            $params['wx_url'] = $optional['wx_url'];
        }
        if (!empty($optional['alipay_url'])) {
            $params['alipay_url'] = $optional['alipay_url'];
        }
        $params['sign'] = $this->sign($params, $secretKey);
        $params['risk_info'] = $optional['risk_info'] ?? json_encode(['ip' => $ip = Support\get_client_ip()]);
        return $params;
    }

    /**
     * @param array $data
     * @param string $secretKey
     * @return string
     */
    public function sign(array $data, string $secretKey)
    {
        $signData = '';
        ksort($data);
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $value = json_encode($v);
            } else {
                $value = $v;
            }
            if ($value) {
                $signData .= '&' . $k . '=' . $value;
            }
        }

        $signData = ltrim($signData, '&');
        return md5($signData . $secretKey);
    }
}
